import numpy as np
import matplotlib.pyplot as plt

file_to_read='gl-latlong-1km-landcover.bsq'
# data1=np.fromfile(file_to_read) # wrong data type
num_pix_y=43200
num_pix_x=21600

map_data=(np.memmap(file_to_read)).reshape(num_pix_x,num_pix_y)

map_data
plt.imshow(map_data[::50,::50])


plt.show()

def is_water(lon:float,lat:float):
    """ Tell the location is water or not

    Args:
        lon (float): the longtitude: -180 : 180, deg
        lat (float): the latitude:   -90  : 90, deg
    """
    py=abs(lat-90)/(num_pix_y/180.0)
    px=abs(lon+180)/(num_pix_x/360.0)

